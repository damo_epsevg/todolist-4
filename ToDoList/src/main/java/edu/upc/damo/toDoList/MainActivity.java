package edu.upc.damo.toDoList;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    private Boolean b=true;
    private EditText nouText;
    private ListView listView;
    private List<CharSequence> dades =new ArrayList<CharSequence>();

    private ArrayAdapter<CharSequence> adaptador;
    private Teclat teclat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llista);

        carregaDades();
        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void carregaDades(){

        String [] dadesEstatiques = getResources().getStringArray(R.array.dadesEstatiques);

        Collections.addAll(dades, dadesEstatiques);
    }


    private void inicialitza(){

        nouText= (EditText) findViewById(R.id.nouText);
        listView = (ListView) findViewById(R.id.llista);

        teclat = new Teclat(this);

        nouText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                switch (teclat.accio(actionId, keyEvent)) {
                    case Teclat.OK:
                        nouText(textView.getText().toString());
                        textView.setText("");
                        return true;
                }

                return true;
            }
        });

         adaptador = new ArrayAdapter<CharSequence>(this,
                R.layout.fila,
                R.id.dia,
                dades);
        listView.setAdapter(adaptador);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Toast.makeText(getBaseContext(),"ItemClick",Toast.LENGTH_SHORT).show();
               parent.setSelection(position);
               view.requestFocus();
               ((TextView) view).setTextColor(ColorStateList.valueOf(android.R.color.holo_blue_light));

            }
        });

        listView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(),"ItemSelected",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getBaseContext(),"NothingSelected",Toast.LENGTH_SHORT).show();

            }
        });

    }



    private void nouText(String text) {
        adaptador.add(text);
     }
}
